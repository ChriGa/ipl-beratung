<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer startFade" role="contentinfo">
	<div class="footer-wrap">				
		<div class="row-fluid">								
			<?php if ($this->countModules('footnav')) : ?>
			<div class="span8 footnav">
				<div class="module_footer position_footnav">
					<jdoc:include type="modules" name="footnav" style="none" />
				</div>			
			</div>
			<?php endif ?>				
			<div class="span12 footer">
				<jdoc:include type="modules" name="footer" style="none" />
			</div>		
		</div>		
	</div>
</footer>
<div id="copyright" class="fullwidth">
	<div class="copyWrapper innerwidth">
		<div class="logo--small__footer">
	      	<img src="/images/ipl-beratung-logo-small.png" alt="IPL-Beratung Logo">
	    </div>
	    <div class="footer--text">
		    <?php print ($lang == " de-de") ? '<p class="footer--text__request">Fragen Sie uns - Direkt</p>' : '<p class="footer--text__request">Ask us directly</p>';?>
		    <p class="phone--footer">+49 - (0) 89 - 92 77 69 42</p>
		    <p class="addr--footer">Schatzbogen 54, 81829 München</p>
		    <p class="email--footer"><a href="mailto:service@ipl-beratung.de">service(at)ipl-beratung.de</a></p>
		</div>
		<?php if($lang == " de-de") : ?>	
			<p class="imprLinks"><a class="imprLink" href="/de/impressum.html" title="Impressum FIRMA">Impressum</a> | <a class="imprLink" href="/de/datenschutz.html">Datenschutz</a></p>
			<p><a class="imprLink" href="https://ipl-mag.de/" target="_blank">IPL-Magazin</a></p>
		<?php else: ?>
			<p class="imprLinks"><a class="imprLink" href="/en/imprint.html" title="Impressum FIRMA">Imprint</a> | <a class="imprLink" href="/en/privacy-policy.html">Privacy Policy</a></p>
			<p><a class="imprLink" href="https://ipl-mag.de/" target="_blank">IPL-Magazin</a></p>		
		<?php endif;?>
	</div>
	<p class="date--company">&copy; <?php print date("Y");?> IPL-Beratung GmbH</p>
</div>	
		