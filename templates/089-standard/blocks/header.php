<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
	
<header id="header" class="fullwidth">
  <div class="innerwidth ">
	<div class="row-fluid">        			
		<div class="span6">
			<a class="brand" href="<?php echo $this->baseurl; ?>">
			<?php echo $logo; ?>
				<?php if ($this->params->get('sitedescription')) : ?>
					<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
				<?php endif; ?>
			</a>
		</div>
		<div class="span6">
			<div class="vcard clr">	
				<h3 class="vcardHeader">vcard Header</h3>
					<p class="fn">Fimra / Name</p>
					<p class="tel "><a class="" href="tel:+555111">Tel: null achtneun perfect</a></p>
					<p class="adr"><span class="street-address">Strasse 0</span><br />
						<span class="postal-code">80000 </span><span class="region">Dorf bei der Stadt</span>
					 </p>
			</div>
		</div>	          
	</div>
  </div>
</header>