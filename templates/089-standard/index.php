<?php
/**
 * @author   	cg@089webdesign.de
 */
 
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
$lang = " " . strtolower(JFactory::getLanguage()->getTag());

?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS ?>
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/woff2" href="/templates/089-standard/fontsmontserrat-bold-webfont.ttf">
	<?php 	/*
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/woff2" href="/templates/089-template/fonts/XXX.woff2">		
	*/ ?>	
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	<link href="/templates/089-standard/css/normalize.css" rel="stylesheet" type="text/css" />
	<link href="/templates/089-standard/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="/templates/089-standard/css/styles.css" rel="stylesheet" type="text/css" />		
</head>

<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass . $lang; ?>">
	<!-- Body -->
	<?php if($frontpage) : ?>
		<div id="preload-Container">
			<div class="preLoader">
				<div class="corp-square corp-square__green"><p>I</p></div>
				<div class="corp-square corp-square__red"><p>P</p></div>
				<div class="corp-square corp-square__blue"><p>L</p></div>
				<p class="string">Beratung</p>
			</div>
		</div>
	<?php endif;?>
		<div id="wrapper" class="fullwidth site_wrapper">
			<?php			

			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');

			// including breadcrumb
			//include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');							
									
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including footer
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />
	<script type="text/javascript" src="<?php print '/templates/' . $this->template . '/js/jquery.lazy.min.js';?>"></script>

	<script type="text/javascript">

		jQuery(document).ready(function() {
			<?php if($frontpage) : //CG: preLoader: ?>
				jQuery(window).load(function(){
					jQuery('body').addClass('rdy');
					if(jQuery('body').hasClass('rdy')) {
<?php /*				setTimeout(function(){
							jQuery('#preload-Container').fadeOut(function(){
								jQuery(this).remove();
							});
						}, 2000); */ ?>
				
						jQuery('#preload-Container').fadeOut(function(){
							jQuery(this).remove();
						});					
					}
				});
			<?php endif;?>

			<?php if($clientMobile) : ?>
				<?php //mobile menu open/close ?>
				jQuery('.btn-navbar').click("on", function() {
					jQuery('.nav-collapse.collapse').toggleClass('openMenu');
					jQuery('button.btn-navbar').toggleClass('btn-modify');
					jQuery('#wrapper').toggleClass('blackend');
				});
			<?php endif; ?>
			<?php if(!$clientMobile || $frontpage && $isTablet): ?>
				jQuery(window).scroll(function(){
					jQuery(".item-page.start").css("background-position","0% " + ( 0 + (jQuery(this).scrollTop() / 1.3)) + "px");
				    var scrollPercent = (500 - window.scrollY) / 500;
				    var topScroll = ((jQuery(this).scrollTop() / 1.7)) + "px";

				    jQuery(".startWrapper--content").css({top: topScroll, opacity: scrollPercent});

				});
			<?php //CG: scroll Ani ?>
				jQuery(window).scroll(function() {
				    var windowBottom = jQuery(this).scrollTop() + jQuery(this).innerHeight();

				    <?php $offsetMinusValue = 200; if($currentMenuID == 166) { $offsetMinusValue = 500;} //CG manche UNterseiten brauchen unterschiedliche Values, werdens mehr wie 2, dann Array! ?>

				    jQuery(".startFade").each(function() {
				        var objectBottom = (jQuery(this).offset().top + jQuery(this).outerHeight() - <?php print $offsetMinusValue;?> ); <?php // hier minus 150 damit es nicht so spät im Viewport erst faded, je nachdem Wert erhöhen, dann faded es "früher" ?>
				        if (objectBottom < windowBottom) { 
				            if (jQuery(this).css("opacity")==0) {
				            	jQuery(this).fadeTo(800,1); 
				            	jQuery(this).addClass('add-Ani');
				            }
				        }
				        if(jQuery(window).scrollTop() == 0 ) {
				        	jQuery(this).css("opacity", "0");
				        	jQuery(this).removeClass('add-Ani');	
				        } 
				    });
				}).scroll();
			<?php endif;?>
			<?php //CG: sticky menu ?>
			<?php if(!$clientMobile) : ?>
				jQuery(window).scroll(function () {
					(jQuery(this).scrollTop() > 85) ? jQuery('.navbar-wrapper').addClass('sticky') : jQuery('.navbar-wrapper').removeClass('sticky');
				});			
			<?php endif;?>

			<?php //CG: lazy:?>
			<?php $thresholdValue = 0;
				(!$clientMobile) ? $thresholdValue = "-50" : $thresholdValue = "0"; ?>
				jQuery('.lazy').Lazy({
				    scrollDirection: 'vertical',
				 <?php if($isPhone) { print "effect: 'fadeIn',\n effectTime: 500,\n"; } else 
				    { print "defaultImage: '/images/ipl-beratung-logo-placeholder.png',"; } ?>
				    threshold: <?php print $thresholdValue; ?>,
				    visibleOnly: true,
				    onError: function(element) {
				        console.log('error loading ' + element.data('src'));
				    }
				});

				setTimeout(function(){ <?php //CG: wg Scroll Ani: in mitten der page reload erfordert scroll to Top, delay, weil anders scheint es nicht zu gehen ?>
					jQuery(window).scrollTop(0);
				}, 700);

				jQuery('.breadcrumb li:nth-child(3) .pathway').on('click', function(e){ <?php //CG: parent-Menues nicht klickbar machen im breadcrumb ?>
							e.preventDefault();
				}); 		
				jQuery('li.parent>a').on('click', function(event){
						event.preventDefault();
						return false;
				});
	});

	</script>	
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>	
</body>
</html>
